lapis = require "lapis"
json = require "json"
util = require "lapis.util"
colors = require "ansicolors"
from_json = util.from_json
to_json = util.to_json

try = (t) ->
  ok,err = pcall t.do
  if not ok
    t.catch err
  t.finally! if t.finally

import respond_to from require "lapis.application"
import json_params from require "lapis.application"

class App extends lapis.Application
  @before_filter =>
    ngx.req.read_body()
    reqBody = ngx.req.get_body_data()
    if reqBody ~= nil
      try
        do: ->
          @reqBody = from_json reqBody
        catch: (e) ->
          -- print 'error!',e
          reqBody = to_json @req.params_post
          @reqBody = @req.params_post
    else
      reqBody = to_json @req.params_post
      @reqBody = @req.params_post
    if reqBody ~= '{}'
      print colors("%{bright}%{cyan}Request body:%{yellow}\n" .. reqBody .. "\n")

  [index: "*"]: respond_to {
  	POST: json_params => 
  		json: @reqBody
  	GET: =>
  	  @title = "Mirror Server (Lapis) - GET"
  	  @html ->
  	  	h1 "Mirror Server (Lapis)"
  	  	h2 "GET"
  	  	p "Use POST for real mirroring"
  	  	h3 "Parsed URL"
  	  	ul ->
  	  	  li ->
  	  	  	b "Scheme: "
  	  	  	text @req.parsed_url.scheme
  	  	  li ->
  	  	  	b "Host: "
  	  	  	text @req.parsed_url.host
  	  	  li ->
  	  	  	b "Port: "
  	  	  	text @req.parsed_url.port
  	  	  li ->
  	  	  	b "Path: "
  	  	  	text @req.parsed_url.path
  	  	  li ->
  	  	  	b "Query: "
  	  	  	text @req.parsed_url.query
  	  	h3 "Headers"
  	  	ul ->
  	  	  for key, value in pairs @req.headers
  	  	  	li ->
  	  	  	  b key
  	  	  	  text " " .. value
  	  	h3 "Params"
  	  	ul ->
  	  	  for key, value in pairs @req.params_get
  	  	  	li ->
  	  	  	  b key .. ": "
  	  	  	  text " " .. value
  }