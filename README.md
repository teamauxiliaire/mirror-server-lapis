# README #

A simple mirror (echo) server written in Lapis/Moonscript.

### How do I get set up? ###

* Make sure your Openresty server is up and running
* Compile moonscript with `moonc *.moon`
* Start Lapis with `lapis server`

### Usage ###

The server currently supports GET and POST requests.

GET requests simply responded with a page containing the request details.

POST requests responded with a JSON response containing the input parameters. POST types supported:

* form-data
* x-www-form-urlencoded
* raw JSON (application/json)